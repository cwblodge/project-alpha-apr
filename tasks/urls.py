from django.urls import path
from .views import create_task, TaskList

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", TaskList, name="show_my_tasks"),
]
