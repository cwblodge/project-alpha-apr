from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from .forms import TaskForm
from . import models


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.assignee = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/task_create.html", context)


@login_required
def TaskList(request):
    tasks = models.Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/mytasks.html", context)
